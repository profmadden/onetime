//
//  Accelerometer.m
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import "Accelerometer.h"

@implementation Accelerometer

- (int)generateSeed{

    //printf("ACCEL X %d Y %d Z %d \n", self.x, self.y, self.z);
    
    return self.x * self.y * self.z;
    
}

@end
