//
//  AppDelegate.h
//  OneTime
//
//  Created by Patrick Madden on 3/31/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Universe.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) Universe *universe;



@end

@protocol OTPNavDelegate
-(void)toImport;
-(void)toDecode;
@end
