//
//  AppDelegate.m
//  OneTime
//
//  Created by Patrick Madden on 3/31/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "AppDelegate.h"


@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize universe;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
	universe = [[Universe alloc] init];  // Create the shared instance
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	
	[universe savePadInformation];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSURL *)applicationDocumentsDirectory
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options
{
	if ((url != nil) && ([url path] != nil))
	{
        
        UIViewController <OTPNavDelegate>*vc = (UIViewController <OTPNavDelegate>*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
        while ([vc presentedViewController] != nil)
        {
            vc = (UIViewController <OTPNavDelegate>*)[vc presentedViewController];
        }

        if ([[[url scheme] lowercaseString] isEqualToString:@"file"])
        {
            NSLog(@"Importing %@", [url path]);
            NSError *err;
            
            NSURL *doc = [self applicationDocumentsDirectory];
            // NSLog(@"Doc dir is %@", [doc path]);
            
            NSURL *final = [doc URLByAppendingPathComponent:[url lastPathComponent]];
            NSLog(@"Moving %@", [final path]);
            
            if (![[NSFileManager defaultManager] moveItemAtURL:url toURL:final error:&err])
            {
                NSLog(@"File import error %@", err);
            }
            
            // [[[Universe sharedInstance] delegate] incomingPad:final];
            [[Universe sharedInstance] setImportURL:final];
            [vc toImport];
        }
        if ([[[url scheme] lowercaseString] isEqualToString:@"otp"])
        {
            NSLog(@"Open up the decoder!");
            [[Universe sharedInstance] setDecodeURL:url];
            [vc toDecode];
        }
    }
	
	return YES;
}


@end
