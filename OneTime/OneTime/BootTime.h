//
//  BootTime.h
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BootTime : NSObject

- (int)generateSeed;

@end
