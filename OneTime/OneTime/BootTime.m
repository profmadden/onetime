//
//  BootTime.m
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BootTime.h"

#include <sys/types.h>
#include <sys/sysctl.h>

#define MIB_SIZE 2

@implementation BootTime

- (int)generateSeed{
    
    int mib[MIB_SIZE];
    size_t size;
    struct timeval  boottime;
    NSDate *bootDate;
    
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    int timeInSecondsInt = (int) timeInSeconds;
    
    mib[0] = CTL_KERN;
    mib[1] = KERN_BOOTTIME;
    size = sizeof(boottime);
    
    if (sysctl(mib, MIB_SIZE, &boottime, &size, NULL, 0) != -1)
    {
        bootDate = [NSDate dateWithTimeIntervalSince1970:
                    boottime.tv_sec + boottime.tv_usec / 1.e6];
    }
    
    int returnValue =  timeInSecondsInt - (int)boottime.tv_sec;
    
    return returnValue;
    
}

@end






