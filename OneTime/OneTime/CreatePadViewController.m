//
//  CreatePadViewController.m
//  OneTime
//
//  Created by Patrick Madden on 11/16/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "CreatePadViewController.h"
#import <sys/time.h>
#define EVAL 1

@interface CreatePadViewController ()

@end

@implementation CreatePadViewController
@synthesize contactName;
@synthesize pad, padSize;
@synthesize randomCount;
@synthesize generate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizer:)];
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapGesture];
    
    /* Motion Logic */
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = .2;
    self.motionManager.gyroUpdateInterval = .2;
    
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                                 [self outputAccelertionData:accelerometerData.acceleration];
                                                 if(error){
                                                     
                                                     NSLog(@"%@", error);
                                                 }
                                             }];
    
    [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue]
                                    withHandler:^(CMGyroData *gyroData, NSError *error) {
                                        [self outputRotationData:gyroData.rotationRate];
                                    }];
    
    /* Generation Allocation */
    
    self.padGenerator = [[PadGenerator alloc] init];
    self.touchLocation = [[TouchLocation alloc] init];
    self.accelerometer = [[Accelerometer alloc] init];
    self.gyroscope = [[Gyroscope alloc] init];
    self.bootTime = [[BootTime alloc] init];
    
    self.padGenerator.touchLocation = self.touchLocation;
    self.padGenerator.accelerometer = self.accelerometer;
    self.padGenerator.gyroscope = self.gyroscope;
    self.padGenerator.bootTime = self.bootTime;
    
    //self.selectedSize = 1024*1024; // 10k pad
    self.selectedSize = 256;
    
    pad = nil;
    randomCount = 0;
    
    [[Universe sharedInstance] setDelegate:self];
    
    // Set the random number generator seed
    srandomdev();
    elapsedTime = 0;

}

-(void)viewDidAppear:(BOOL)animated
{
    [contactName setText:@"Contact"];
    [generate setEnabled:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(NSURL *)docdir:(NSString *)fn
{
    NSArray *dirs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *res = [NSURL URLWithString:fn relativeToURL:[dirs objectAtIndex:0]];
    
    return res;
}

-(IBAction)createContactAndPad:(id)sender
{
    // Must have a valid contact name
    if ([[contactName text] length] < 1)
    {
        NSLog(@"No contact name!");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Contact Name" message:@"Please input the contact name for this pad." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];

        return;
    }
    
    NSLog(@"Create a new pad for the named contact, and add it to the archive.");
    NSString *prefixString = @"otp";
    
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
    NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@.onetimepad", prefixString, guid];
    
    NSLog(@"uniqueFileName: '%@'", uniqueFileName);
    NSURL *padURL = [self docdir:uniqueFileName];
    NSLog(@"URL is %@", padURL);
    NSString *path = [padURL path];
    NSLog(@"Path is %@", path);
    FILE *fp = fopen([path UTF8String], "wb");
    NSLog(@"FP is %p", fp);
    
    // [self.padGenerator generatePad:self.selectedSize];
    
    //fprintf(fp, "This is some data");
    
    //sliding scale for pad sizes: TODO
    
    int n;
    for (n =0; n < self.selectedSize; n++) {
        fprintf(fp, "%c", pad[n]);
    }
    
    fflush(fp);
    fclose(fp);
    
    // Mark file for non-backup
    // [self addSkipBackupAttributeToItemAtPath:path];
    
#if 0

#endif
    
    PadManager *pm = [[PadManager alloc] init];
    [pm setPadSize:self.selectedSize];
    [pm setPadFileName:uniqueFileName]; //changing to full path also makes fopen fail
    [pm setConsumedIndex:0];
    [pm setContactName:[contactName text]];
    [[[Universe sharedInstance] outgoingPads] addObject:pm];
    
    NSUInteger k = [[[Universe sharedInstance] incomingPads] count];
    NSLog(@"SIZE OF THE INCOMING PAD ARRAY %lu", (unsigned long)k);
    NSLog(@"CONTACT NAEM: %@", pm.contactName);
    
    
    //Start transmission of pad
    
    Exporter *exporter = [[Exporter alloc] init];
    [exporter exportFile:[sender bounds] path:path viewController:self];
    
}
- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString
{
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(success)
    {
        NSLog(@"Excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    else
    {
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    
    return success;
}

// A touch will make a pass across the pad,
// to exclusive or whatever might be there already with
// new random numbers taken from random (better randomization,
// and seeded by the crypto-friendly srandomdev), and
// also random numbers generated by rand (less randomization,
// but we will use touch location, gyro, and accelerometer for
// this seeding).
-(void)touch:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"Generate pad touch!");
    [contactName resignFirstResponder];
    
    unsigned char character;
    int i;
    padSize = self.selectedSize;

#if EVAL
    
    struct timeval start, stop;
    gettimeofday(&start, NULL);
    
#endif
    
    long rseed = 5;
    for (UITouch *t in touches)
    {
        CGPoint p = [t locationInView:nil];
        
        rseed = rseed ^ (unsigned int)p.x ^ (unsigned int)p.y ^ (unsigned int) (p.x * p.y);
        rseed = rseed ^ (int)[t timestamp];
        rseed = rseed ^ (rseed << 8);

        srand((unsigned) rseed);
        rseed = rand();
        NSLog(@"Rseed %lx %f %f", rseed, p.x, p.y);
    }

    if (!pad)
        pad = (unsigned char *)malloc(sizeof(unsigned char)*padSize);
    
    // This loop runs with each tap to the
    // screen.
    for (i = 0; i < padSize; i++)
    {
        // The medium quality random number
        character = random();
        // The bad one, seeded by this touch...
        unsigned int r2 = rand();
        // And a good random number
        unsigned int r3 = arc4random();
        // XOR them together with what was on the
        // pad in the last pass.  We only use the
        // high three bytes of the low quality
        // random number (low byte cycles)
        character = ((self.pad[i] ^ character) ^
                    (r2 >> 24) ^
                    (r2 >> 16) ^
                    (r2 >> 8) ^
                    r3) & 0xFF;
        // And store the combination into the
        // pad...
        self.pad[i] = character;
    }
    ++randomCount;
    
    if (randomCount > 20)
    {
        NSLog(@"Ok, that's enough!");
        [generate setEnabled:YES];
    }

#if EVAL
    
    gettimeofday(&stop, NULL);
    long elapsed = (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec;
    printf("Total pad generation time: (%ld)\n", elapsed);
    elapsedTime += elapsed;
    
    
#endif
    
}

-(void)outputAccelertionData:(CMAcceleration)acceleration{
    
    self.accelerometer.x = acceleration.x * 100000;
    self.accelerometer.y = acceleration.y * 100000;
    self.accelerometer.z = acceleration.z * 100000;
    
}

-(void)outputRotationData:(CMRotationRate)rotation{
    
    self.gyroscope.x = rotation.x * 100000;
    self.gyroscope.y = rotation.y * 100000;
    self.gyroscope.z = rotation.z * 100000;
    
}

- (void)tapGestureRecognizer:(UIGestureRecognizer *)recognizer {
    
    CGPoint tappedPoint = [recognizer locationInView:self.view];
    CGFloat xCoordinate = tappedPoint.x * 1000;
    CGFloat yCoordinate = tappedPoint.y * 1000;
    
    self.touchLocation.x = xCoordinate;
    self.touchLocation.y = yCoordinate;
    
    
}

- (IBAction)done:(UIStoryboardSegue *)segue
{
    NSLog(@"Return to main view controller");
}

-(void)incomingPad:(NSURL *)url
{
    NSLog(@"View controller receives incoming pad.");
    
    printf("------------receiving pad now at url-------------\n");
    
    NSLog(@"DELEGATE IMPORT %@", [url path]);
    
    FILE * fp = fopen([[url path] UTF8String], "rb+");
    
    printf("\n FILE USED TO OPEN: %s \n", [[url path] UTF8String]);
    
    NSLog(@"FP is %p", fp);
    
    int incomingPadSize = 0;
    unsigned char character;
    
    printf("INCOMING PAD CONTENTS:\n");
    
    while (!feof(fp)) {
        
        if (fscanf(fp, "%c", &character) != 1) {
            break;
        }
        
        printf("%x\n", character);
        incomingPadSize++;
        
    }
    
    printf("INCOMING PAD SIZE: %d\n", incomingPadSize);
    
    
    //Get contact name from user store in pm contact name [TODO]
    
    PadManager *pm = [[PadManager alloc] init];
    [pm setPadSize:incomingPadSize];
    [pm setContactName:[contactName text]];
    [pm setPadFileName:[url path]];
    [pm setConsumedIndex:0];
    
    printf("\n");
    NSLog(@"FILE NAME BEFORE SAVING: %@", [pm padFileName]);
    printf("\n");
    
    
    [[[Universe sharedInstance]incomingPads] addObject:pm];
    [[Universe sharedInstance] savePadInformation];
    
    printf("\n");
    NSLog(@"FILE NAME AFTER SAVING: %@", [pm padFileName]);
    printf("\n");
    
    
}


-(void)toImport
{
    NSLog(@"View controller receives incoming pad.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toImport" sender:self];
}

-(void)toDecode
{
    NSLog(@"View controller receives encrypted text.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toDecode" sender:self];
}

@end
