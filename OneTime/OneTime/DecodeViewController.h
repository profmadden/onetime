//
//  DecodeViewController.h
//  OneTime
//
//  Created by Kevin Di Benedetto on 8/25/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Universe.h"

@interface DecodeViewController : UIViewController<UITextViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (retain, nonatomic) IBOutlet UITextView *encryptedText;
@property (retain, nonatomic) IBOutlet UITextView *decryptedText;
@property (retain, nonatomic) IBOutlet UITableView *padTable;
@property (nonatomic) int selectedPad;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *padRightConstraint;

@end
