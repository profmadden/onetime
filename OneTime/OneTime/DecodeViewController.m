//
//  DecodeViewController.m
//  OneTime
//
//  Created by Kevin Di Benedetto on 8/25/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "DecodeViewController.h"
#define LDBG 1

@interface DecodeViewController ()

@end

@implementation DecodeViewController
@synthesize padRightConstraint;
@synthesize padTable;
@synthesize encryptedText;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [padTable setDataSource:self];
    [padTable setDelegate:self];
    [encryptedText setDelegate:self];
    
    self.selectedPad = 0;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([[Universe sharedInstance] decodeURL] != nil)
    {
        [encryptedText setText:[[[Universe sharedInstance] decodeURL] absoluteString]];
        [[Universe sharedInstance] setDecodeURL:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)checkPad:(id)sender
{
    if ([[[Universe sharedInstance] incomingPads] count] <= self.selectedPad)
    {
        return;
    }

    
    PadManager *pm = [[[Universe sharedInstance] incomingPads] objectAtIndex:self.selectedPad];
    NSURL *url = [self docdir:[[pm padFileName] lastPathComponent]];
    NSLog(@"URL %@ %@", url, [url path]);
    
    NSLog(@"Select pad %@ size %d consumed %d", [pm padFileName], [pm padSize], [pm consumedIndex]);
    
    FILE *fp = fopen([[url path] UTF8String], "rb");
    if (fp == NULL)
    {
        NSLog(@"Could not open the file");
    }
    else
    {
        NSLog(@"Opened successfully.");
        fclose(fp);
    }
}

-(IBAction)dismissKeyboard:(id)sender
{
    [encryptedText resignFirstResponder];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        PadManager *pm = [[[Universe sharedInstance] incomingPads] objectAtIndex:[indexPath row]];
        [[[Universe sharedInstance] incomingPads] removeObject:pm];
        [[NSFileManager defaultManager] removeItemAtURL:[self docdir:[pm padFileName]] error:nil];
        // [self.dataArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    NSLog(@"Select row %d", (int)[indexPath row]);
    
    self.selectedPad = (int)[indexPath row];
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *c = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"padCell"];
    if (c == nil)
    {
        c = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"padCell"];
    }
    
    PadManager *pm = [[[Universe sharedInstance] incomingPads] objectAtIndex:[indexPath row]];
    [[c textLabel] setText:[NSString stringWithFormat:@"%@ %@", [pm contactName], [pm padFileName]]];
    [[c detailTextLabel] setText:[NSString stringWithFormat:@"%d bytes used, %d available", [pm consumedIndex], [pm padSize] - [pm consumedIndex]]];
    return c;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[Universe sharedInstance] incomingPads] count];
}

-(NSURL *)docdir:(NSString *)fn
{
    NSArray *dirs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *res = [NSURL URLWithString:fn relativeToURL:[dirs objectAtIndex:0]];
    
    return res;
}

-(void)reportError:(NSString *)title detail:(NSString *)detail
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:detail preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}

static int hex(char c)
{
    if ((c >= '0') && (c <= '9'))
        return c - '0';
    if ((c >= 'A') && (c <= 'F'))
        return 10 + (c - 'A');
    if ((c >= 'a') && (c <= 'f'))
        return 10 + (c - 'a');
    
    return 0;
}

- (IBAction)decrypt:(id)sender
{
    if ([[[Universe sharedInstance] incomingPads] count] <= self.selectedPad)
    {
        [self reportError:@"Select a pad!" detail:@"You must select a pad to use for encoding your message."];
        return;
    }
    PadManager *pm = [[[Universe sharedInstance] incomingPads] objectAtIndex:self.selectedPad];
    
    NSString *encryptedMessage = [encryptedText text];
    const unsigned char *utf8 = (unsigned char *)[encryptedMessage cStringUsingEncoding:NSASCIIStringEncoding];
    
    if (strncasecmp("otp://", (char *)utf8, 6) == 0)
    {
        NSLog(@"Move past the OTP.");
        utf8 +=  6;
    }
   
    int encryptedMessageLength = (int)strlen((char *) utf8);
    if ((encryptedMessageLength % 2) != 0)
    {
        [self reportError:@"Message Length!" detail:@"The length of the incoming message is not correct.  There should be an even number of hex digits."];
        return;
    }

    if (encryptedMessageLength < 10)
    {
        [self reportError:@"Message too short!" detail:@"There should be more bytes here!"];
        return;
    }
    
    int index;
    if (sscanf((char *) utf8, "%08x", &index) != 1)
    {
        [self reportError:@"Message too short!" detail:@"There should be more bytes here!"];
        return;
    }
    NSLog(@"Incoming message offset: %d\n", index);
    if (index != [pm consumedIndex])
    {
        [self reportError:@"Message Index Error!" detail:@"The index for this message does not match the current pad.  Please make sure you're using the correct pad, and you have not missed an incoming message."];
        return;
    }
    
   
    // Move to the start of the encrypted characters
    utf8 += 8;
    encryptedMessageLength = (int) strlen((char *) utf8);
    unsigned char message[encryptedMessageLength/2 + 1];
    
    int i, j;
    j = 0;
    for (i = 0; i < encryptedMessageLength; i += 2)
    {
        message[j] = (hex(utf8[i]) << 4) + hex(utf8[i + 1]);
        NSLog(@"Receive %x from %c %c", message[j], utf8[i], utf8[i + 1]);
        ++j;
    }
    int length = j - 1;
    
    
    unsigned char checkSum = 0;
    for (i = 0; i < length; ++i)
        checkSum ^= message[i];
    
    NSLog(@"Computed checksum %x", checkSum);
    if (checkSum != message[length])
    {
        [self reportError:@"Checksum error" detail:@"There should be more bytes here!"];
        return;
    }

    printf("full message end\n");
    
    // NSURL *url = [self docdir:[pm padFileName]];
    NSURL *url = [self docdir:[[pm padFileName] lastPathComponent]];
    NSLog(@"URL %@ %@", url, [url path]);

    FILE *fp = fopen([[url path] UTF8String], "rb+");
    if (!fp)
    {
        [self reportError:@"Pad reading error" detail:@"Something went wrong with reading the pad."];
        printf("ERROR INSIDE DECODE: %s\n", strerror(errno));
        NSLog(@"Pad should be %@", [pm padFileName]);
        return;
    }
    
    unsigned char padBuffer[length];
    fseek(fp, index, SEEK_SET);
    fread(padBuffer, sizeof(unsigned char), j - 1, fp);
    unsigned char clearText[j];
    
    for (i = 0; i < j - 1; ++i)
    {
        unsigned char c = message[i] ^ padBuffer[i];
        NSLog(@"Decrypt %x with pad %x to %x", message[i], padBuffer[i], c);
        clearText[i] = c;
    }
    clearText[i] = '\0';
    [_decryptedText setText:[NSString stringWithUTF8String:(unsigned char *)clearText]];
    
    // Zero out the pad we have used
    for (i = 0; i < length; ++i)
        padBuffer[i] = 0;
    fseek(fp, [pm consumedIndex], SEEK_SET);
    fwrite(padBuffer, sizeof(unsigned char), length, fp);
    fclose(fp);
    
    [pm setConsumedIndex:[pm consumedIndex] + length];
    [[Universe sharedInstance] savePadInformation];
    [padTable reloadData];
    
    fclose(fp);

}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    // textView.text = @"";
}



-(IBAction)togglePadSelect:(id)sender
{
    NSLog(@"Toggle pad visibility");
    static float constraint = 0;
    
    if ([padRightConstraint constant] == 0)
    {
        [padRightConstraint setConstant:constraint];
        [UIView animateWithDuration:.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    else
    {
        if (constraint == 0)
            constraint = [padRightConstraint constant];
        [padRightConstraint setConstant:0];
        [UIView animateWithDuration:.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)toImport
{
    NSLog(@"View controller receives incoming pad.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toImport" sender:self];
}

-(void)toDecode
{
    NSLog(@"View controller receives encrypted text.");
    if ([[Universe sharedInstance] decodeURL] != nil)
    {
        [encryptedText setText:[[[Universe sharedInstance] decodeURL] absoluteString]];
        [[Universe sharedInstance] setDecodeURL:nil];
    }

    
    // [[Universe sharedInstance] setImportURL:url];
    // [self performSegueWithIdentifier:@"toDecode" sender:self];
}
@end
