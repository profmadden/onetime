//
//  EncodeViewController.h
//  OneTime
//
//  Created by Patrick Madden on 5/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#include <string.h>
#include <stdio.h>

#import "Universe.h"
#import "HexDisplay.h"
#import "Exporter.h"

@interface EncodeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource, UITextViewDelegate,MFMessageComposeViewControllerDelegate>
@property (nonatomic, strong) IBOutlet UITableView *padTable;
@property (nonatomic, strong) IBOutlet UITextView *hexDisplay;
@property (nonatomic, strong) IBOutlet UITextView *clearText;
@property (nonatomic, strong) IBOutlet UIAlertController *alert;
@property (nonatomic) int selectedPad;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *padRightConstraint;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *bottomConstraint;
@property (nonatomic, strong) IBOutlet UIButton *currentPad;
@end
