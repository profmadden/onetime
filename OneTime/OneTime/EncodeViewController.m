//
//  EncodeViewController.m
//  OneTime
//
//  Created by Patrick Madden on 5/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "EncodeViewController.h"

#include "errno.h"

@interface EncodeViewController ()

@end

@implementation EncodeViewController
@synthesize padTable;
@synthesize hexDisplay;
@synthesize clearText;
@synthesize padRightConstraint;
@synthesize bottomConstraint;
@synthesize currentPad;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[padTable setDelegate:self];
	[padTable setDataSource:self];
    [clearText setDelegate:self];
    
    self.selectedPad = 0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    // Hide the pad selection
    [self togglePadSelect:nil];
    [currentPad setTitle:@"PAD" forState:UIControlStateNormal];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}


-(void)reportError:(NSString *)title detail:(NSString *)detail
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:detail preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


// We allow deleting rows
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        PadManager *pm = [[[Universe sharedInstance] outgoingPads] objectAtIndex:[indexPath row]];
        [[[Universe sharedInstance] outgoingPads] removeObject:pm];
        [[NSFileManager defaultManager] removeItemAtURL:[self docdir:[pm padFileName]] error:nil];
        // [self.dataArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
        [[Universe sharedInstance] savePadInformation];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
	NSLog(@"Select row %d", (int)[indexPath row]);
	
    self.selectedPad = (int)[indexPath row];
    PadManager *pm = [[[Universe sharedInstance] outgoingPads] objectAtIndex:[indexPath row]];
    
    [currentPad setTitle:[pm contactName] forState:UIControlStateNormal];
    [self togglePadSelect:nil];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *c = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"padCell"];
	if (c == nil)
	{
        c = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"padCell"];
	}

    PadManager *pm = [[[Universe sharedInstance] outgoingPads] objectAtIndex:[indexPath row]];
	[[c textLabel] setText:[NSString stringWithFormat:@"%@ %@", [pm contactName], [pm padFileName]]];
    [[c detailTextLabel] setText:[NSString stringWithFormat:@"%d bytes used, %d available", [pm consumedIndex], [pm padSize] - [pm consumedIndex]]];

	return c;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [[[Universe sharedInstance] outgoingPads] count];
}


-(NSURL *)docdir:(NSString *)fn
{
	NSArray *dirs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
	NSError *err;
	[[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
	NSURL *res = [NSURL URLWithString:fn relativeToURL:[dirs objectAtIndex:0]];
	
	return res;
}

-(IBAction)export:(id)sender
{
	PadManager *pm = [[[Universe sharedInstance] outgoingPads] objectAtIndex:0];
	NSLog(@"Export pad %@ %@", [pm contactName], [pm padFileName]);
	NSURL *url = [self docdir:[pm padFileName]];
	Exporter *exporter = [[Exporter alloc] init];
	[exporter exportFile:[sender bounds] path:[url path] viewController:self];
}



static char *lookup = "0123456789ABCDEF";


-(void)setHex:(int)length data:(UInt8 *)data
{
    char *tmp = (char *)malloc(length * 2 + 1);
    int idx = 0;
    for (int i = 0; i < length; ++i)
    {
        tmp[idx++] = lookup[(data[i] & 0xF0) >> 4];
        tmp[idx++] = lookup[(data[i] & 0xF)];
    }
    tmp[idx] = '\0';
    
    // [self setText:[NSString stringWithFormat:@"otp://%s", tmp]];
    free(tmp);
}
-(IBAction)encrypt:(id)sender
{
    if ([[[Universe sharedInstance] outgoingPads] count] <= self.selectedPad)
    {
        [self reportError:@"Select a pad!" detail:@"You must select a pad to use for encoding your message."];
        return;
    }
    PadManager *pm = [[[Universe sharedInstance] outgoingPads] objectAtIndex:self.selectedPad];
    
	NSString *clear = [clearText text];
	UInt8 *utf8 = (UInt8 *)[clear UTF8String];
	int length = (int) strlen((char *)utf8);

    // Need a pad character for each char we intend to encrypt
    if (length > ([pm padSize] - [pm consumedIndex]))
    {
        [self reportError:@"Out of pad!" detail:@"There is not enough space left on this pad to encode the message."];
        return;
    }
    
    // The encoded text will need 6 bytes for the otp://, 8 bytes for the index,
    // 2 bytes for each character, 2 bytes
    // for the checksum, and one more byte for the null terminator
    char data[6 + 8 + 2*strlen((char *)utf8) + 2 + 1];
    
    unsigned char index[4];
    unsigned char checkSum;
    int indexLength = 4;


    int startIndex = [pm consumedIndex];
    sprintf(data, "otp://%08x", startIndex);

   
    //PadManager *pm2 = [[[Universe sharedInstance] outgoingPads] objectAtIndex:1];
    int i;
    printf("PAD SIZE: %d\n", [pm padSize]);
    printf("CONSUMED INDEX: %d\n", startIndex);
    
    NSLog(@"FILE INSIDE ENCRYUTPYION %@", [pm padFileName]);
    //NSLog(@"File %@", [pm2 padFileName]);
    
    NSURL *url = [self docdir:[pm padFileName]];
    FILE *fp = fopen([[url path] UTF8String], "rb+");
    

    if (!fp)
    {
        [self reportError:@"Pad reading error" detail:@"Something went wrong with reading the pad."];
        return;
    }
    
#if 0
    
    int j;
    for (j = [pm consumedIndex]; j < length; j++) {
        
        fscanf(fp, "%c", &currentChar);
        printf("%x\n", currentChar);
        
    }
    
#endif
    
    unsigned char padBuffer[length];
    fseek(fp, [pm consumedIndex], SEEK_SET);
    fread(padBuffer, sizeof(unsigned char), length, fp);
    
    checkSum = 0;
    // Index into encrypted output string.
    // 6 to move past otp, plus 8 for the index
    int j = 6 + 8;
    for (i = 0; i < length; ++i)
    {
        unsigned char c = utf8[i] ^ padBuffer[i];

        data[j++] = lookup[((c & 0xF0)>>4)];
        data[j++] = lookup[(c & 0x0F)];
        checkSum ^= c;
    }
    data[j++] = lookup[((checkSum & 0xF0)>>4)];
    data[j++] = lookup[checkSum & 0xF];
    data[j] = '\0';
    
    [hexDisplay setText:[NSString stringWithUTF8String:data]];

    // Zero out the pad we have used
    for (i = 0; i < length; ++i)
        padBuffer[i] = 0;
    fseek(fp, [pm consumedIndex], SEEK_SET);
    fwrite(padBuffer, sizeof(unsigned char), length, fp);
    fclose(fp);
    
    [pm setConsumedIndex:[pm consumedIndex] + length];
    [[Universe sharedInstance] savePadInformation];
    [padTable reloadData];

    [self sendSMS:nil];
    
#if 0
    unsigned char *fullPad = (unsigned char *)malloc(([pm padSize] * (sizeof(unsigned char))));
    fseek(fp, 0, SEEK_SET);
    fread(fullPad, sizeof(unsigned char), [pm padSize], fp);
    
    printf("STORED PAD FULL\n");
    int u;
    for (u = 0 ; u < [pm padSize]; u++) {
        printf("%x\n", fullPad[u]);
    }
    
    
    printf("PADBUFFER\n");
    int k;
    for (k = 0; k < length; k++) {
        
        printf("%x\n", padBuffer[k]);
    
    }
    printf("length %d\n", length);
    
    //fread(fp, numberofbytes);
    //seek back and zero out
    //universe save pad info
    printf("GIVEN MESSAGE START\n");
    for (i = 0; utf8[i] != '\0'; ++i) {
        printf("%x\n", utf8[i]);
    }
    printf("GIVEN MESSAGE END\n");
    
	for (i = 0; utf8[i] != '\0'; ++i)
	{
		data[i] = utf8[i] ^ padBuffer[i]; // Encode with XOR
	}
    
    unsigned char zero = '0';
    
    printf("MESSAGE ENCRYPTED START\n");
    for (i = 0; data[i] != '\0'; ++i) {
        printf("%x\n", data[i]);
    }
    printf("MESSAGE ENCRYPTED END\n");
    
    
    printf("DECRYPTED MESSAGE START\n");
    
    for (i = 0; data[i] != '\0'; ++i) {
        
        printf("%x\n", data[i] ^ padBuffer[i]);
    
    }
    
    printf("DECRYPTED MESSAGE END\n");
    
    //Load index and data into outgoing message
    for (i = 0; i < indexLength; i++) {
        outgoingMessage[i] = index[i];
    }
    
    for (i = 0; data[i] != '\0'; i++) {
        outgoingMessage[i + indexLength] = data[i];
    }
    
    //Compute checksum
    
    checkSum[0] = outgoingMessage[0] ^ outgoingMessage[1];
    
    for (i = 2; i < (outgoingMessageLength - 1); i++) {
        checkSum[0] = checkSum[0] ^ outgoingMessage[i];
    }
    
    printf("CHECKSUM = %x\n", checkSum[0]);
    
    //Load checksum into outgoing message
    
    outgoingMessage[length + indexLength] = checkSum[0];
    
    printf("OUTGOING MESSAGE START\n");
    
    for (i = 0; i < length + 5; i++) {
        printf("%x\n", outgoingMessage[i]);
    }
    
    printf("OUTGOING MESSAGE END\n");
    
#if 0
    
    unsigned char * secondSum = (unsigned char *)malloc(sizeof(unsigned char));
    
    for (i = 0; i < length + (indexLength - 1); i++) {
        secondSum[0] = outgoingMessage[i] ^ outgoingMessage[i + 1];
    }
    
    
    
    printf("VERIFY CHECKSUM %x\n", secondSum[0]);
    
#endif
    
    fseek(fp, [pm consumedIndex], SEEK_SET);
    //fwrite(&zero, sizeof(unsigned char), length, fp);
    
    for (i = 0; i < length; i++) {
        fputc(zero, fp);
    }
    
    fseek(fp, 0, SEEK_SET);
    fread(fullPad, sizeof(unsigned char), [pm padSize], fp);
    
    printf("FULL PAD AFTER ZERO OUT\n");
    for (u = 0 ; u < [pm padSize]; u++) {
        printf("%x\n", fullPad[u]);
    }
    
    //Set pm to new consumed index
    //endcode to persist upon app close
    
    pm.consumedIndex = pm.consumedIndex + length;
    
    [[Universe sharedInstance] savePadInformation];
    
    printf("NEW Consumed index: %d\n", [pm consumedIndex]);
    
	[hexDisplay setHex:outgoingMessageLength data:outgoingMessage];
    
    [UIPasteboard generalPasteboard].string = hexDisplay.text;
    
    //self.alert = [UIAlertController alertControllerWithTitle:@"Message Status" message:@"The encrypted message has been copied to the clipboard." preferredStyle:UIAlertControllerStyleAlert];
    
    //UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
    
   // [self.alert addAction:defaultAction];
    //[self presentViewController:self.alert animated:YES completion:nil];


    fflush(fp);
    fclose(fp);
    
    [padTable reloadData];
    [[Universe sharedInstance] savePadInformation];
    
    [self sendSMS:nil];
#endif
}

- (void) textViewDidBeginEditing:(UITextView *) textView {
    // [textView setText:@""];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
-(IBAction)dismissKeyboard:(id)sender
{
	[clearText resignFirstResponder];
}

-(IBAction)togglePadSelect:(id)sender
{
    NSLog(@"Toggle pad visibility");
    static float constraint = 0;
    
    if ([padRightConstraint constant] == 0)
    {
        [padRightConstraint setConstant:constraint];
        [UIView animateWithDuration:.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    else
    {
        if (constraint == 0)
            constraint = [padRightConstraint constant];
        [padRightConstraint setConstant:0];
        [UIView animateWithDuration:.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    // scrollView.contentInset = contentInsets;
    // scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    [bottomConstraint setConstant:kbSize.height];
    [UIView animateWithDuration:.3 animations:^{
          [self.view layoutIfNeeded];
    }];
    
#if 0
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
#endif
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [bottomConstraint setConstant:0];
    [UIView animateWithDuration:.3 animations:^{
          [self.view layoutIfNeeded];
    }];
    
#if 0
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
#endif
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendSMS:(id)sender
{
    
    if(![MFMessageComposeViewController canSendText])
    {
        [self reportError:@"SMS Not Supported" detail:@"Sorry!  This device can't send an SMS message!"];
        return;
    }
    
    NSArray *recipents = nil;
    NSString *message = [hexDisplay text];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}


-(void)toImport
{
    NSLog(@"View controller receives incoming pad.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toImport" sender:self];
}

-(void)toDecode
{
    NSLog(@"View controller receives encrypted text.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toDecode" sender:self];
}

@end
