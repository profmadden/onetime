//
//  Exporter.h
//  Infinite Looper
//
//  Created by Patrick Madden on 3/28/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
#import "Universe.h"

@interface Exporter : NSObject<UIDocumentInteractionControllerDelegate>
@property (nonatomic, strong) NSString *thePath;
@property (nonatomic, strong) UIViewController *theViewController;
-(IBAction)exportFile:(CGRect)r path:(NSString*)path viewController:(UIViewController *)viewController;
@property (nonatomic, strong) UIDocumentInteractionController *docController;

@end
