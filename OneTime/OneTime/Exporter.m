//
//  Exporter.m
//  Infinite Looper
//
//  Created by Patrick Madden on 3/28/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "Exporter.h"
#define LDBG 0

@implementation Exporter
@synthesize thePath;
@synthesize theViewController;
@synthesize docController;

-(IBAction)exportFile:(CGRect)r path:(NSString*)path viewController:(UIViewController *)viewController
{
	//Here the url is the document URL that you want to open (or you want to apply open in functionality)
	thePath = path;
	theViewController = viewController;


	NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
	FILE *fp = fopen([path UTF8String], "r");
	NSLog(@"File is %p", fp);
	fclose(fp);
	NSURL *URL = [[NSBundle mainBundle] URLForResource:@"engineering" withExtension:@"pdf"];
	
	docController = [UIDocumentInteractionController interactionControllerWithURL:url];
	[docController retain];
	// docController = [[UIDocumentInteractionController alloc] init];
	// [docController setURL:url];
	NSLog(@"URL is %@", url);
	[docController setDelegate:self];
	// [openInView setHidden:YES];

	
	// BOOL result = [docController presentOptionsMenuFromRect:r inView:openInView animated:YES];
	r.origin.x = r.origin.y = 0;

	// [docController presentPreviewAnimated:YES];
	[docController presentOptionsMenuFromRect:r inView:[viewController view] animated:YES];

#if 0
	if (URL) {
		// Initialize Document Interaction Controller
		UIDocumentInteractionController *documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
		
		// Configure Document Interaction Controller
		[documentInteractionController setDelegate:self];
		
		// Preview PDF
		r.origin.x = r.origin.y = 0;
		[documentInteractionController presentOptionsMenuFromRect:r inView:[viewController view] animated:YES];
	}
#endif
}

- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)interactionController
{
	return theViewController;
}

- (void)documentInteractionControllerWillPresentOpenInMenu:(UIDocumentInteractionController *)controller
{
	if (LDBG) NSLog(@"Will present open in menu");
	// [openInView setHidden:NO];
}

-(void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
	if (LDBG) NSLog(@"Dismissed open in controller");
	// [openInView setHidden:YES];
}

-(void)documentInteractionController:(UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
	NSLog(@"Finished sending the document.  Maybe nag?");
}


#pragma mark - QLPreviewControllerDataSource


// Returns the number of items that the preview controller should preview
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)previewController
{
	NSInteger numToPreview = 1;
	if (LDBG) NSLog(@"Preview number of items");
	
	return numToPreview;
}

- (void)previewControllerDidDismiss:(QLPreviewController *)controller
{
	// if the preview dismissed (done button touched), use this method to post-process previews
	NSLog(@"Preview dismiss");
}

// returns the item that the preview controller should preview
- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx
{
	NSURL *fileURL = nil;
		
	return fileURL;
}

@end
