//
//  Gyroscope.h
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Gyroscope : NSObject

@property (nonatomic) int x;
@property (nonatomic) int y;
@property (nonatomic) int z;

- (int)generateSeed;

@end
