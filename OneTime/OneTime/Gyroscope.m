//
//  Gyroscope.m
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import "Gyroscope.h"

@implementation Gyroscope

-(int)generateSeed{

    //printf("GYRO X %d Y %d Z %d \n", self.x, self.y, self.z);
    
    return self.x * self.y * self.z;
    
}

@end
