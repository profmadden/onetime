//
//  HexDisplay.h
//  OneTime
//
//  Created by Patrick Madden on 5/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HexDisplay : UITextView
-(void)setHex:(int)length data:(UInt8 *)data;
@end
