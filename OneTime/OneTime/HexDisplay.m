//
//  HexDisplay.m
//  OneTime
//
//  Created by Patrick Madden on 5/9/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "HexDisplay.h"

@implementation HexDisplay

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

static char *lookup = "0123456789ABCDEF";


-(void)setHex:(int)length data:(UInt8 *)data
{
	char *tmp = (char *)malloc(length * 2 + 1);
	int idx = 0;
	for (int i = 0; i < length; ++i)
	{
		tmp[idx++] = lookup[(data[i] & 0xF0) >> 4];
		tmp[idx++] = lookup[(data[i] & 0xF)];
	}
	tmp[idx] = '\0';
	
    [self setText:[NSString stringWithFormat:@"otp://%s", tmp]];
	free(tmp);
}
@end
