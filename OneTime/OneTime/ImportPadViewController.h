//
//  ImportPadViewController.h
//  OneTime
//
//  Created by Patrick Madden on 11/16/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Universe.h"

@interface ImportPadViewController : UIViewController
@property (nonatomic, strong) IBOutlet UITextField *contactName;

@end
