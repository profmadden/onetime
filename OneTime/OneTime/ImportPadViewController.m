//
//  ImportPadViewController.m
//  OneTime
//
//  Created by Patrick Madden on 11/16/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ImportPadViewController.h"

@interface ImportPadViewController ()

@end

@implementation ImportPadViewController
@synthesize contactName;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    NSLog(@"Import view appeard, UNIVERSE url is %@", [[Universe sharedInstance] importURL]);
}


-(IBAction)import:(id)sender
{
    if ([[contactName text] length] < 1)
    {
        NSLog(@"No contact name!");
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Contact Name" message:@"Please input the contact name for this pad." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    printf("------------receiving pad now at url-------------\n");
    NSURL *url = [[Universe sharedInstance] importURL];

    NSLog(@"DELEGATE IMPORT %@", [url path]);
    
#if 0
    FILE * fp = fopen([[url path] UTF8String], "rb+");
    
    printf("\n FILE USED TO OPEN: %s \n", [[url path] UTF8String]);
    
    NSLog(@"FP is %p", fp);
    
    int incomingPadSize = 0;
    unsigned char character;
    
    printf("INCOMING PAD CONTENTS:\n");
    
    while (!feof(fp)) {
        
        if (fscanf(fp, "%c", &character) != 1) {
            break;
        }
        
        printf("%x\n", character);
        incomingPadSize++;
        
    }
    
    printf("INCOMING PAD SIZE: %d\n", incomingPadSize);
#endif
    
    //Get contact name from user store in pm contact name [TODO]
    unsigned long long fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:[url path] error:nil] fileSize];

    PadManager *pm = [[PadManager alloc] init];
    [pm setPadSize:(int)fileSize];
    [pm setContactName:[contactName text]];
    [pm setPadFileName:[url path]];
    [pm setConsumedIndex:0];
    
    printf("\n");
    NSLog(@"FILE NAME BEFORE SAVING: %@", [pm padFileName]);
    printf("\n");
    
    
    [[[Universe sharedInstance]incomingPads] addObject:pm];
    [[Universe sharedInstance] savePadInformation];
    
    printf("\n");
    NSLog(@"FILE NAME AFTER SAVING: %@", [pm padFileName]);
    printf("\n");


}


-(void)toImport
{
    NSLog(@"View controller receives incoming pad.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toImport" sender:self];
}

-(void)toDecode
{
    NSLog(@"View controller receives encrypted text.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toDecode" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
