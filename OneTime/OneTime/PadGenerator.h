//
//  PadGenerator.h
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BootTime.h"
#import "TouchLocation.h"
#import "Accelerometer.h"
#import "Gyroscope.h"

@interface PadGenerator : NSObject

@property (nonatomic) char * pad;
@property (nonatomic) int padSize;
@property (nonatomic) int seedStored;
@property (nonatomic, strong) BootTime * bootTime;
@property (nonatomic, strong) TouchLocation * touchLocation;
@property (nonatomic, strong) Accelerometer * accelerometer;
@property (nonatomic, strong) Gyroscope * gyroscope;

- (void)generatePad:(int)padSize;

@end
