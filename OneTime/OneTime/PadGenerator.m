//
//  PadGenerator.m
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import "PadGenerator.h"

@implementation PadGenerator

-(void)generatePad:(int)padSize{


    
    /* Initialization */
    self.padSize = padSize;
    self.pad = malloc(sizeof(char) * padSize);
    
    /* Population */
    int i, j, seedSelected;
    unsigned char byte, character;
    
    /* Set initial seed using boot time module */
    
    srandom([self.bootTime generateSeed]);
    
    /* Generate inital random array of character bytes. */
    
    for (i = 0; i < padSize; i++) {
        byte = random(); //The default seed is 1 with no argument.
        self.pad[i] = byte;
    }
    
#if 1
    
    printf("--ORIGINAL STRING---\n");
    
    for (i = 0; i < padSize; i++) {
        printf("%x", (unsigned char)self.pad[i]);
        printf("\n");
    }
    
#endif
    
    for (j = 0; j < 10; j++) {
        
        for (i = 0; i < padSize; i++) {
            character = random();
            //printf("%d\n", character);
            character = self.pad[i] ^ character;
            self.pad[i] = character;
            
        }
        
        seedSelected = random() % 4;
        
        printf("SEED SOURCE %d\n", seedSelected);
        
        if(seedSelected == 0){
        
            srandom([self.bootTime generateSeed] + j);
            
        }
        else if (seedSelected == 1){
        
            srandom([self.touchLocation generateSeed] + j);
            
        }
        else if (seedSelected == 2){
        
            srandom([self.accelerometer generateSeed] + j);
        }
        else if (seedSelected == 3){
        
            srandom([self.gyroscope generateSeed] + j);
            
        }
        
    }

#if 1
    
    printf("--RANDOM GENERATED STRING--\n");
    
    for (i = 0; i < padSize; i++) {
        printf("%x", (unsigned char)self.pad[i]);
        printf("\n");
    }
    
#endif
    
    
}

@end
