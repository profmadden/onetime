//
//  PadManager.h
//  OneTime
//
//  Created by Patrick Madden on 4/3/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PadManager : NSObject<NSCoding>
@property (nonatomic, strong) NSString *contactName;
@property (nonatomic) int consumedIndex;
@property (nonatomic) int padSize;
@property (nonatomic, strong) NSString *padFileName;

@end
