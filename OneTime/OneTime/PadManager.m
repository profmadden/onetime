//
//  PadManager.m
//  OneTime
//
//  Created by Patrick Madden on 4/3/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "PadManager.h"

@implementation PadManager
@synthesize contactName, consumedIndex, padSize, padFileName;

-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super init];
	if (self)
	{
		contactName = [aDecoder decodeObjectForKey:@"contactName"];
		consumedIndex = (int)[aDecoder decodeIntegerForKey:@"consumedIndex"];
		padSize = (int)[aDecoder decodeIntegerForKey:@"padSize"];
		padFileName = [aDecoder decodeObjectForKey:@"padFileName"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
	[aCoder encodeObject:contactName forKey:@"contactName"];
	[aCoder encodeInteger:consumedIndex forKey:@"consumedIndex"];
	[aCoder encodeInteger:padSize forKey:@"padSize"];
	[aCoder encodeObject:padFileName forKey:@"padFileName"];
}
@end
