//
//  TouchLocation.h
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol TouchLocationDelegate;

@interface TouchLocation : UIView

@property (nonatomic) int x;
@property (nonatomic) int y;
@property (nonatomic, strong) IBOutlet id<TouchLocationDelegate> delegate;
- (int)generateSeed;

@end

@protocol TouchLocationDelegate
-(void)touch:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
@end

