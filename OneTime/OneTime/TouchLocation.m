//
//  TouchLocation.m
//  generation
//
//  Created by Kevin Di Benedetto on 8/20/16.
//  Copyright © 2016 Kevin Di Benedetto. All rights reserved.
//

#import "TouchLocation.h"

@implementation TouchLocation
@synthesize delegate;

- (int)generateSeed{
    
    return (self.x * self.y);
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [delegate touch:touches withEvent:event];
}

@end
