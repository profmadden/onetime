//
//  Universe.h
//  OneTime
//
//  Created by Patrick Madden on 4/3/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PadManager.h"

@protocol IncomingPadDelegate;

@interface Universe : NSObject
@property (nonatomic, strong) NSMutableArray *incomingPads;
@property (nonatomic, strong) NSMutableArray *outgoingPads;
@property (nonatomic) int fileNumber;
@property (nonatomic, strong) id<IncomingPadDelegate> delegate;
@property (nonatomic, strong) NSURL *importURL;
@property (nonatomic, strong) NSURL *decodeURL;

+(Universe *)sharedInstance;
-(void)loadPadInformation;
-(void)savePadInformation;

@end

@protocol IncomingPadDelegate
-(void)incomingPad:(NSURL *)url;
@end
