//
//  Universe.m
//  OneTime
//
//  Created by Patrick Madden on 4/3/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "Universe.h"

@implementation Universe
@synthesize incomingPads, outgoingPads;
@synthesize fileNumber;
static Universe *singleton = nil;
@synthesize delegate;
@synthesize importURL;
@synthesize decodeURL;

-(id)init
{
	if (singleton)
		return singleton;
	
	self = [super init];
	if (self)
	{
		singleton = self;
		[self loadPadInformation];
	}
	return self;
}


+(Universe *)sharedInstance
{
	return singleton;
}


-(NSURL *)appsup:(NSString *)fn
{
	NSArray *dirs = [[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
	NSError *err;
	[[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
	NSURL *res = [NSURL URLWithString:fn relativeToURL:[dirs objectAtIndex:0]];
	
	return res;
}

-(void)loadPadInformation
{
	NSURL *archivePath = [self appsup:@"onetime.archive"];

	if ([[NSFileManager defaultManager] fileExistsAtPath:[archivePath path]])
	{
		NSLog(@"Loading in prior pad information --- starting loading back of pad files");
	
		NSData *data;
		data = [NSData dataWithContentsOfURL:archivePath];
		NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
		incomingPads = [unarchiver decodeObjectForKey:@"incomingPads"];
		outgoingPads = [unarchiver decodeObjectForKey:@"outgoingPads"];
		fileNumber = [unarchiver decodeIntForKey:@"fileNumber"];
	}
	else
	{
		incomingPads = [[NSMutableArray alloc] init];
		outgoingPads = [[NSMutableArray alloc] init];
		fileNumber = 0;
	}
}

-(void)savePadInformation
{
	NSURL *archivePath = [self appsup:@"onetime.archive"];
	NSMutableData *data = [[NSMutableData alloc] init];
	NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];

	NSLog(@"Saving pad information");
	
	[archiver encodeObject:incomingPads forKey:@"incomingPads"];
	[archiver encodeObject:outgoingPads forKey:@"outgoingPads"];
	[archiver encodeInt:fileNumber forKey:@"fileNumber"];
	[archiver finishEncoding];
	[data writeToURL:archivePath atomically:YES];
}
@end
