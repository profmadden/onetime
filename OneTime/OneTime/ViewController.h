//
//  ViewController.h
//  OneTime
//
//  Created by Patrick Madden on 3/31/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import "TouchLocation.h"
#import "PadGenerator.h"
#import "Accelerometer.h"
#import "Gyroscope.h"
#import "BootTime.h"
#import "Universe.h"
#import "Exporter.h"

@interface ViewController : UIViewController<IncomingPadDelegate>
@property (nonatomic, strong) IBOutlet UITextField *contactName;
@property (nonatomic, strong) TouchLocation * touchLocation;
@property (nonatomic, strong) PadGenerator * padGenerator;
@property (nonatomic, strong) Accelerometer * accelerometer;
@property (nonatomic, strong) Gyroscope * gyroscope;
@property (nonatomic, strong) BootTime * bootTime;
@property (nonatomic, strong) CMMotionManager * motionManager;
@property (nonatomic) int selectedSize;
@property (nonatomic, strong) IBOutlet UITextView *infoView;

-(IBAction)createContactAndPad:(id)sender;

@end
