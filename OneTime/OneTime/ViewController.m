//
//  ViewController.m
//  OneTime
//
//  Created by Patrick Madden on 3/31/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize contactName;
@synthesize infoView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    /* Tap Logic */
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizer:)];
    [self.view setUserInteractionEnabled:YES];
    [self.view addGestureRecognizer:tapGesture];
    
    /* Motion Logic */
    
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = .2;
    self.motionManager.gyroUpdateInterval = .2;
    
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue]
                                             withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                                 [self outputAccelertionData:accelerometerData.acceleration];
                                                 if(error){
                                                     
                                                     NSLog(@"%@", error);
                                                 }
                                             }];
    
    [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue currentQueue]
                                    withHandler:^(CMGyroData *gyroData, NSError *error) {
                                        [self outputRotationData:gyroData.rotationRate];
                                    }];
    
    /* Generation Allocation */
    
    self.padGenerator = [[PadGenerator alloc] init];
    self.touchLocation = [[TouchLocation alloc] init];
    self.accelerometer = [[Accelerometer alloc] init];
    self.gyroscope = [[Gyroscope alloc] init];
    self.bootTime = [[BootTime alloc] init];
    
    self.padGenerator.touchLocation = self.touchLocation;
    self.padGenerator.accelerometer = self.accelerometer;
    self.padGenerator.gyroscope = self.gyroscope;
    self.padGenerator.bootTime = self.bootTime;

    self.selectedSize = 256;
    

    [infoView setText:@"Welcome to XOR Assist!\n\nThis app performes one-time pad encryption using large random pads; the fundamental mathematical operation is exclusive-or (XOR).  You can create a random pad on this device, and then share it with a friend using AirDrop.  Use the pad to encrypt a message, and then send the encrypted message using the iMessage application; your friend can decrypt the message.\n\nSource code for this application is available, so that you can see for yourself how it works.  Please visit http://www.cs.binghamton.edu/~pmadden for more details!"];
   
    [[Universe sharedInstance] setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSURL *)docdir:(NSString *)fn
{
    NSArray *dirs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *res = [NSURL URLWithString:fn relativeToURL:[dirs objectAtIndex:0]];
    
    return res;
}

-(IBAction)createContactAndPad:(id)sender
{
    
	NSLog(@"Create a new pad for the named contact, and add it to the archive.");
	NSString *prefixString = @"otp";
	
	NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString] ;
	NSString *uniqueFileName = [NSString stringWithFormat:@"%@_%@.onetimepad", prefixString, guid];
	
	NSLog(@"uniqueFileName: '%@'", uniqueFileName);
    NSURL *pad = [self docdir:uniqueFileName];
    NSLog(@"URL is %@", pad);
    NSString *path = [pad path];
    NSLog(@"Path is %@", path);
    FILE *fp = fopen([path UTF8String], "wb");
    NSLog(@"FP is %p", fp);
    
    [self.padGenerator generatePad:self.selectedSize];
    
    //fprintf(fp, "This is some data");
    
    //sliding scale for pad sizes: TODO
    
    int n;
    for (n =0; n < self.selectedSize; n++) {
        fprintf(fp, "%c", self.padGenerator.pad[n]);
    }

	fflush(fp);
    fclose(fp);

#if 0
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Contact Name" message:@"Please input the contact name for this pad." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Input" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
    [alert addAction:action];
#endif
    
	PadManager *pm = [[PadManager alloc] init];
	[pm setPadSize:self.selectedSize];
	[pm setPadFileName:uniqueFileName]; //changing to full path also makes fopen fail
	[pm setConsumedIndex:0];
	[pm setContactName:[contactName text]];
	[[[Universe sharedInstance] outgoingPads] addObject:pm];
    
    NSUInteger k = [[[Universe sharedInstance] incomingPads] count];
    NSLog(@"SIZE OF THE INCOMING PAD ARRAY %lu", (unsigned long)k);
    NSLog(@"CONTACT NAEM: %@", pm.contactName);
    
    
    //Start transmission of pad
    
    Exporter *exporter = [[Exporter alloc] init];
    [exporter exportFile:[sender bounds] path:path viewController:self];

}

-(void)outputAccelertionData:(CMAcceleration)acceleration{
    
    self.accelerometer.x = acceleration.x * 100000;
    self.accelerometer.y = acceleration.y * 100000;
    self.accelerometer.z = acceleration.z * 100000;
    
}

-(void)outputRotationData:(CMRotationRate)rotation{
    
    self.gyroscope.x = rotation.x * 100000;
    self.gyroscope.y = rotation.y * 100000;
    self.gyroscope.z = rotation.z * 100000;
    
}

- (void)tapGestureRecognizer:(UIGestureRecognizer *)recognizer {
    
    CGPoint tappedPoint = [recognizer locationInView:self.view];
    CGFloat xCoordinate = tappedPoint.x * 1000;
    CGFloat yCoordinate = tappedPoint.y * 1000;
    
    self.touchLocation.x = xCoordinate;
    self.touchLocation.y = yCoordinate;
    
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"Performing segue %@", segue.identifier);
}

- (IBAction)done:(UIStoryboardSegue *)segue
{
	NSLog(@"Return to main view controller");
}

-(void)toImport
{
    NSLog(@"View controller receives incoming pad.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toImport" sender:self];
}

-(void)toDecode
{
    NSLog(@"View controller receives encrypted text.");
    
    // [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toDecode" sender:self];
}

-(void)incomingPad:(NSURL *)url
{
    NSLog(@"View controller receives incoming pad.");
    
    [[Universe sharedInstance] setImportURL:url];
    [self performSegueWithIdentifier:@"toImport" sender:self];

#if 0
    printf("------------receiving pad now at url-------------\n");
    
    NSLog(@"DELEGATE IMPORT %@", [url path]);
    
    FILE * fp = fopen([[url path] UTF8String], "rb+");
    
    printf("\n FILE USED TO OPEN: %s \n", [[url path] UTF8String]);
    
    NSLog(@"FP is %p", fp);
    
    int incomingPadSize = 0;
    unsigned char character;
    
    printf("INCOMING PAD CONTENTS:\n");
    
    while (!feof(fp)) {
        
        if (fscanf(fp, "%c", &character) != 1) {
            break;
        }
        
        printf("%x\n", character);
        incomingPadSize++;
        
    }
    
    printf("INCOMING PAD SIZE: %d\n", incomingPadSize);
    
    
    //Get contact name from user store in pm contact name [TODO]
    
    PadManager *pm = [[PadManager alloc] init];
    [pm setPadSize:incomingPadSize];
    [pm setContactName:[contactName text]];
    [pm setPadFileName:[url path]];
    [pm setConsumedIndex:0];
    
    printf("\n");
    NSLog(@"FILE NAME BEFORE SAVING: %@", [pm padFileName]);
    printf("\n");
    
    
    [[[Universe sharedInstance]incomingPads] addObject:pm];
    [[Universe sharedInstance] savePadInformation];
    
    printf("\n");
    NSLog(@"FILE NAME AFTER SAVING: %@", [pm padFileName]);
    printf("\n");
#endif
    
}

- (void)dealloc {
    [super dealloc];
}
@end
